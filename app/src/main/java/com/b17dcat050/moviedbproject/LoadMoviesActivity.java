package com.b17dcat050.moviedbproject;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
<<<<<<< HEAD
import androidx.viewpager.widget.ViewPager;
import androidx.viewpager2.widget.ViewPager2;

=======
>>>>>>> 6c34d5e (Project initialized)
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
<<<<<<< HEAD

import com.b17dcat050.moviedbproject.adapter.ViewPager2Adapter;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;


public class LoadMoviesActivity extends AppCompatActivity {
    private ViewPager2 myViewPager2;
    private ViewPager2Adapter myAdapter;
    private TabLayout myTabLayout;
=======
import com.google.android.material.bottomnavigation.BottomNavigationView;


public class LoadMoviesActivity extends AppCompatActivity {
>>>>>>> 6c34d5e (Project initialized)

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_load_movies);

<<<<<<< HEAD
        // Tab Navigation handling
        myViewPager2 = (ViewPager2) findViewById(R.id.container);
        myTabLayout = (TabLayout) findViewById(R.id.tabs);
        myAdapter = new ViewPager2Adapter(getSupportFragmentManager(), getLifecycle());
        myViewPager2.setAdapter(myAdapter);
        new TabLayoutMediator(myTabLayout, myViewPager2, new TabLayoutMediator.TabConfigurationStrategy() {
            @Override
            public void onConfigureTab(@NonNull TabLayout.Tab tab, int position) {
                switch(position){
                    case 0:
                        tab.setText("POPULAR");
                        break;
                    case 1:
                        tab.setText("NOW-PLAYING");
                        break;
                    case 2:
                        tab.setText("UPCOMING");
                        break;
                    case 3:
                        tab.setText("TOP-RATED");
                        break;
                }
            }
        }).attach();

        // Bottom Navigation Bar handling
        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById((R.id.bottomNavigation));
        MenuItem item = bottomNavigationView.getMenu().findItem(R.id.ic_movie);
        item.setChecked(true);
=======



        // Bottom Navigation Bar handling
        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById((R.id.bottomNavigation));
        Menu menu = bottomNavigationView.getMenu();
        MenuItem menuItem = menu.getItem(0);
        menuItem.setChecked(true);
>>>>>>> 6c34d5e (Project initialized)
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.ic_movie:
                        break;

                    case R.id.ic_list:
                        Intent intent2 = new Intent(getApplicationContext(), SelectGenreActivity.class);
                        startActivity(intent2);
                        break;

                    case R.id.ic_search:
                        Intent intent3 = new Intent(getApplicationContext(), SearchByNameActivity.class);
                        startActivity(intent3);
                        break;

                    case R.id.ic_favorite:
                        Intent intent4 = new Intent(getApplicationContext(), ShowFavoritesActivity.class);
                        startActivity(intent4);
                        break;
                }
                return false;
            }
        });

    }

}