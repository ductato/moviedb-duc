package com.b17dcat050.moviedbproject;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
<<<<<<< HEAD
import androidx.recyclerview.widget.RecyclerView;
=======
>>>>>>> 6c34d5e (Project initialized)

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
<<<<<<< HEAD
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;


import com.b17dcat050.moviedbproject.adapter.ListViewAdapter;
import com.b17dcat050.moviedbproject.api.Client;
import com.b17dcat050.moviedbproject.api.Service;
import com.b17dcat050.moviedbproject.model.Genres;
import com.b17dcat050.moviedbproject.model.GenresResponse;

import com.b17dcat050.moviedbproject.model.Movie;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;
import java.util.List;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SelectGenreActivity extends AppCompatActivity {
    ListView lv;
    private Service movieService;
    private List<Genres> genreList;
    private ListViewAdapter adapter;

=======

import com.google.android.material.bottomnavigation.BottomNavigationView;

public class SelectGenreActivity extends AppCompatActivity {
>>>>>>> 6c34d5e (Project initialized)

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_genre);

<<<<<<< HEAD
        lv = (ListView) findViewById(R.id.listView);
        genreList = new ArrayList<>();

        movieService = Client.getClient().create(Service.class);
        insertGenre();

        // Bottom Navigation Bar handling
        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById((R.id.bottomNavigation));
        MenuItem item = bottomNavigationView.getMenu().findItem(R.id.ic_list);
        item.setChecked(true);
=======


        // Bottom Navigation Bar handling
        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById((R.id.bottomNavigation));
        Menu menu = bottomNavigationView.getMenu();
        MenuItem menuItem = menu.getItem(1);
        menuItem.setChecked(true);
>>>>>>> 6c34d5e (Project initialized)
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.ic_movie:
                        Intent intent1 = new Intent(getApplicationContext(), LoadMoviesActivity.class);
                        startActivity(intent1);
                        break;

                    case R.id.ic_list:
                        break;

                    case R.id.ic_search:
                        Intent intent3 = new Intent(getApplicationContext(), SearchByNameActivity.class);
                        startActivity(intent3);
                        break;

                    case R.id.ic_favorite:
                        Intent intent4 = new Intent(getApplicationContext(), ShowFavoritesActivity.class);
                        startActivity(intent4);
                        break;
                }
                return false;
            }
        });
<<<<<<< HEAD
    }

    private void insertGenre() {
        getGenres().enqueue(new Callback<GenresResponse>() {
            @Override
            public void onResponse(Call<GenresResponse> call, Response<GenresResponse> response) {
                // Got data -> Send to adapter
                GenresResponse genresResponse = response.body();
                for (Genres result : genresResponse.getResults()) {
                        genreList.add(result);
                }
                adapter = new ListViewAdapter(genreList, SelectGenreActivity.this);
                lv.setAdapter(adapter);
                lv.setOnItemClickListener(new AdapterView.OnItemClickListener(){
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id){

                        Genres selItem = (Genres) parent.getItemAtPosition(position);
                        Intent intent = new Intent(lv.getContext(), GenreSearchResultActivity.class);
                        intent.putExtra("genres", selItem );
                        lv.getContext().startActivity(intent);
                    }
                });
            }
            @Override
            public void onFailure(Call<GenresResponse> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private Call<GenresResponse> getGenres() {
        return movieService.getGenres(
                BuildConfig.THE_MOVIE_DB_API_TOKEN
        );
=======

>>>>>>> 6c34d5e (Project initialized)
    }

}