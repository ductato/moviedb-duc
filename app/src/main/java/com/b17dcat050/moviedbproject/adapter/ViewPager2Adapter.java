package com.b17dcat050.moviedbproject.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.b17dcat050.moviedbproject.loadmoviesfragment.PlayingFragment;
import com.b17dcat050.moviedbproject.loadmoviesfragment.PopularFragment;
import com.b17dcat050.moviedbproject.loadmoviesfragment.TopFragment;
import com.b17dcat050.moviedbproject.loadmoviesfragment.UpcomingFragment;

public class ViewPager2Adapter extends FragmentStateAdapter {
    public ViewPager2Adapter(@NonNull FragmentManager fragmentManager, Lifecycle lifecycle) {
        super(fragmentManager, lifecycle);
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        switch(position){
            case 0:
                return new PopularFragment();
            case 1:
                return new PlayingFragment();
            case 2:
                return new UpcomingFragment();
            case 3:
                return new TopFragment();
            default:
                return new PopularFragment();
        }
    }

    @Override
    public int getItemCount() {
        return 4;
    }

}
