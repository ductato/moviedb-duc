package com.b17dcat050.moviedbproject.adapter;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.media.Image;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.b17dcat050.moviedbproject.R;
import com.b17dcat050.moviedbproject.util.FavoriteDbHelper;

import java.util.ArrayList;

public class FavoriteAdapter extends BaseAdapter {
    private Context mContext;
    FavoriteDbHelper dbHelper;
    SQLiteDatabase db;
    private ArrayList<Integer> Id = new ArrayList<Integer>();
    private ArrayList<Integer> MovieId = new ArrayList<Integer>();
    private ArrayList<String> Name = new ArrayList<String>();
    private ArrayList<Bitmap> Image = new ArrayList<Bitmap>();
    private ArrayList<String> Sypnosis = new ArrayList<String>();

    public FavoriteAdapter(Context  context,ArrayList<Integer> MovieId, ArrayList<String> Name, ArrayList<Bitmap> Image, ArrayList<String> Sypnosis)
    {
        this.mContext = context;
        this.Name = Name;
        this.Image = Image;
        this.MovieId= MovieId;
        this.Sypnosis= Sypnosis;;
    }
    @Override
    public int getCount() {
        return Id.size();
    }
    @Override
    public Object getItem(int position) {
        return null;
    }
    @Override
    public long getItemId(int position) {
        return 0;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final  viewHolder holder;
        dbHelper = new FavoriteDbHelper(mContext);
        LayoutInflater layoutInflater;
        if (convertView == null) {
            layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.favorite_list_show_favorites, null);
            holder = new viewHolder();
            holder.movieId = (TextView) convertView.findViewById(R.id.movie_id);
            holder.name = (TextView) convertView.findViewById(R.id.movie_title);
            holder.image = (ImageView) convertView.findViewById(R.id.movie_poster);
            holder.sypnosis = (TextView) convertView.findViewById(R.id.movie_desc);

            convertView.setTag(holder);
        } else {
            holder = (viewHolder) convertView.getTag();
        }
        holder.movieId.setText(MovieId.get(position));
        holder.name.setText(Name.get(position));
        holder.image.setImageBitmap(Image.get(position));
        holder.sypnosis.setText(Sypnosis.get(position));
        return convertView;
    }
    public class viewHolder {
        TextView movieId;
        TextView name;
        ImageView image;
        TextView sypnosis;
    }
}


