package com.b17dcat050.moviedbproject.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.b17dcat050.moviedbproject.R;
import com.b17dcat050.moviedbproject.model.Genres;

import java.util.List;

public class ListViewAdapter extends BaseAdapter {
    Context mContext;
    private List<Genres> mGenre;

    public ListViewAdapter(List<Genres> mGenre, Context mContext) {
        this.mGenre = mGenre;
        this.mContext = mContext;
    }

    public class ViewHolder {
        public TextView tvId;
        public TextView tvName;
    }

    @Override
    public int getCount() {
        return mGenre.size();
    }

    @Override
    public Object getItem(int position) {
        return mGenre.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {

        View view = convertView;
        ViewHolder holder;

        if (convertView == null) {

            LayoutInflater inflater;
            inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            view = inflater.inflate(R.layout.item_list_select_genre, null);

            holder = new ViewHolder();
            holder.tvName = (TextView) view.findViewById(R.id.tvName);

            view.setTag(holder);

        } else
            holder = (ViewHolder) view.getTag();

        holder.tvName.setText(mGenre.get(position).getName());
        return view;
    }
}