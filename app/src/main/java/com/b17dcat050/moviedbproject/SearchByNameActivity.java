package com.b17dcat050.moviedbproject;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
<<<<<<< HEAD
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;
=======
>>>>>>> 6c34d5e (Project initialized)

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
<<<<<<< HEAD
import android.view.View;
import android.widget.SearchView;

import com.b17dcat050.moviedbproject.adapter.PaginationAdapter;
import com.b17dcat050.moviedbproject.adapter.ViewPager2Adapter;
import com.b17dcat050.moviedbproject.api.Client;
import com.b17dcat050.moviedbproject.api.Service;
import com.b17dcat050.moviedbproject.model.Movie;
import com.b17dcat050.moviedbproject.model.MoviesResponse;
import com.b17dcat050.moviedbproject.util.PaginationScrollListener;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchByNameActivity extends AppCompatActivity {
    private String SEARCH_HINT = "Search movies by name";
    private String SEARCH_QUERY;
    PaginationAdapter adapter;
    LinearLayoutManager linearLayoutManager;
    RecyclerView rv;
    private static final int PAGE_START = 1;
    private int TOTAL_PAGES = 2;
    private int currentPage = PAGE_START;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    private Service movieService;
=======

import com.google.android.material.bottomnavigation.BottomNavigationView;

public class SearchByNameActivity extends AppCompatActivity {
>>>>>>> 6c34d5e (Project initialized)

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_by_name);

<<<<<<< HEAD
        //RecyclerView Initializing
        rv = (RecyclerView)findViewById(R.id.main_recycler);
        adapter = new PaginationAdapter(SearchByNameActivity.this);
        linearLayoutManager = new LinearLayoutManager(SearchByNameActivity.this, LinearLayoutManager.VERTICAL, false);
        rv.setLayoutManager(linearLayoutManager);
        rv.setItemAnimator(new DefaultItemAnimator());
        rv.setAdapter(adapter);

        //SearchView handling
        SearchView searchView = (SearchView) findViewById(R.id.searchBar);
        searchView.setQueryHint(SEARCH_HINT);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                SEARCH_QUERY = query;
                adapter.clear();
                rv.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
                    @Override
                    protected void loadMoreItems() {
                        isLoading = true;
                        currentPage += 1;
                        TOTAL_PAGES += 1;
                        loadNextPage();
                    }

                    @Override
                    public int getTotalPageCount() {
                        return TOTAL_PAGES;
                    }

                    @Override
                    public boolean isLastPage() {
                        return isLastPage;
                    }

                    @Override
                    public boolean isLoading() {
                        return isLoading;
                    }
                });

                //init service and load data
                movieService = Client.getClient().create(Service.class);
                loadFirstPage();

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
               adapter.clear();
                return false;
            }
        });

        // Bottom Navigation Bar handling
        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById((R.id.bottomNavigation));
        MenuItem item = bottomNavigationView.getMenu().findItem(R.id.ic_search);
        item.setChecked(true);
=======


        // Bottom Navigation Bar handling
        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById((R.id.bottomNavigation));
        Menu menu = bottomNavigationView.getMenu();
        MenuItem menuItem = menu.getItem(2);
        menuItem.setChecked(true);
>>>>>>> 6c34d5e (Project initialized)
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.ic_movie:
                        Intent intent1 = new Intent(getApplicationContext(), LoadMoviesActivity.class);
                        startActivity(intent1);
                        break;

                    case R.id.ic_list:
                        Intent intent2 = new Intent(getApplicationContext(), SelectGenreActivity.class);
                        startActivity(intent2);
                        break;

                    case R.id.ic_search:
                        break;

                    case R.id.ic_favorite:
                        Intent intent4 = new Intent(getApplicationContext(), ShowFavoritesActivity.class);
                        startActivity(intent4);
                        break;
                }
                return false;
            }
        });

    }

<<<<<<< HEAD
    private void loadFirstPage() {
        searchByName().enqueue(new Callback<MoviesResponse>() {
            @Override
            public void onResponse(Call<MoviesResponse> call, Response<MoviesResponse> response) {
                // Got data -> Send to adapter

                List<Movie> results = fetchResults(response);
                adapter.addAll(results);

                if (currentPage <= TOTAL_PAGES) adapter.addLoadingFooter();
                else isLastPage = true;
            }

            @Override
            public void onFailure(Call<MoviesResponse> call, Throwable t) {
                t.printStackTrace();

            }
        });
    }

    private List<Movie> fetchResults(Response<MoviesResponse> response) {
        MoviesResponse movieResponse = response.body();
        return movieResponse.getResults();
    }

    private void loadNextPage() {
        searchByName().enqueue(new Callback<MoviesResponse>() {
            @Override
            public void onResponse(Call<MoviesResponse> call, Response<MoviesResponse> response) {
                adapter.removeLoadingFooter();
                isLoading = false;

                List<Movie> results = fetchResults(response);
                adapter.addAll(results);

                if (currentPage != TOTAL_PAGES) adapter.addLoadingFooter();
                else isLastPage = true;
            }

            @Override
            public void onFailure(Call<MoviesResponse> call, Throwable t) {
                t.printStackTrace();
                // No handling failure
            }
        });
    }

    private Call<MoviesResponse> searchByName() {
        return movieService.searchByName(
                BuildConfig.THE_MOVIE_DB_API_TOKEN,
                SEARCH_QUERY,
                currentPage
        );
    }

=======
>>>>>>> 6c34d5e (Project initialized)
}