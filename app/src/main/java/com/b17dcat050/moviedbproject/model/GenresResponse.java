package com.b17dcat050.moviedbproject.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GenresResponse implements Parcelable {
    @SerializedName("genres")
    private List<Genres> results;

    public GenresResponse() {
    }

    protected GenresResponse(Parcel in) {
        this.results = in.createTypedArrayList(Genres.CREATOR);
    }

    public List<Genres> getResults() {
        return results;
    }

    public void setResults(List<Genres> results) {
        this.results = results;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.results);
    }

    public static final Creator<GenresResponse> CREATOR = new Creator<GenresResponse>() {
        @Override
        public GenresResponse createFromParcel(Parcel source) {
            return new GenresResponse(source);
        }

        @Override
        public GenresResponse[] newArray(int size) {
            return new GenresResponse[size];
        }
    };

}

