package com.b17dcat050.moviedbproject;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
<<<<<<< HEAD
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.media.Image;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import com.b17dcat050.moviedbproject.adapter.FavoriteAdapter;
import com.b17dcat050.moviedbproject.adapter.PaginationAdapter;
import com.b17dcat050.moviedbproject.model.Movie;
import com.b17dcat050.moviedbproject.model.MoviesResponse;
import com.b17dcat050.moviedbproject.util.FavoriteContract;
import com.b17dcat050.moviedbproject.util.FavoriteDbHelper;
import com.b17dcat050.moviedbproject.util.PaginationScrollListener;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static java.lang.Integer.parseInt;

public class ShowFavoritesActivity extends AppCompatActivity {

    ListView lv;
    private FavoriteDbHelper favoriteDbHelper = new FavoriteDbHelper(this);
    SQLiteDatabase db;
    private ArrayList<Integer> MovieId = new ArrayList<Integer>();
    private ArrayList<String> Name = new ArrayList<String>();
    private ArrayList<Bitmap> Image = new ArrayList<Bitmap>();
    private ArrayList<String> Sypnosis = new ArrayList<String>();


=======

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;

public class ShowFavoritesActivity extends AppCompatActivity {

>>>>>>> 6c34d5e (Project initialized)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_favorites);

<<<<<<< HEAD
        //RecyclerView Initializing
        lv = (ListView)findViewById(R.id.listView);

        // Bottom Navigation Bar handling
        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById((R.id.bottomNavigation));
        MenuItem item = bottomNavigationView.getMenu().findItem(R.id.ic_favorite);
        item.setChecked(true);
=======


        // Bottom Navigation Bar handling
        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById((R.id.bottomNavigation));
        Menu menu = bottomNavigationView.getMenu();
        MenuItem menuItem = menu.getItem(3);
        menuItem.setChecked(true);
>>>>>>> 6c34d5e (Project initialized)
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.ic_movie:
                        Intent intent1 = new Intent(getApplicationContext(), LoadMoviesActivity.class);
                        startActivity(intent1);
                        break;

                    case R.id.ic_list:
                        Intent intent2 = new Intent(getApplicationContext(), SelectGenreActivity.class);
                        startActivity(intent2);
                        break;

                    case R.id.ic_search:
                        Intent intent3 = new Intent(getApplicationContext(), SearchByNameActivity.class);
                        startActivity(intent3);
                        break;

                    case R.id.ic_favorite:
                        break;
                }
                return false;
            }
        });
<<<<<<< HEAD
    }

    @Override
    protected void onResume() {
        displayData();
        super.onResume();
    }
    private void displayData() {
        db = favoriteDbHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from "+FavoriteContract.FavoriteEntry.TABLE_NAME, null );
        MovieId.clear();
        Name.clear();
        Image.clear();
        Sypnosis.clear();

        if (cursor.moveToFirst()) {
            do {
                MovieId.add(parseInt(cursor.getString(cursor.getColumnIndex(FavoriteContract.FavoriteEntry.COLUMN_MOVIEID))));
                Name.add(cursor.getString(cursor.getColumnIndex(FavoriteContract.FavoriteEntry.COLUMN_TITLE)));
//                Image.add(cursor.getString(cursor.getColumnIndex(FavoriteContract.FavoriteEntry.COLUMN_POSTER_PATH)));
                Sypnosis.add(cursor.getString(cursor.getColumnIndex(FavoriteContract.FavoriteEntry.COLUMN_PLOT_SYNOPSIS)));
            } while (cursor.moveToNext());
        }
        FavoriteAdapter adapter = new FavoriteAdapter(ShowFavoritesActivity.this, MovieId, Name, Image, Sypnosis);
        lv.setAdapter(adapter);
        cursor.close();
    }

=======

    }
>>>>>>> 6c34d5e (Project initialized)

}