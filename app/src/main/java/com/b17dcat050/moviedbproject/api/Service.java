package com.b17dcat050.moviedbproject.api;

import com.b17dcat050.moviedbproject.model.GenresResponse;
import com.b17dcat050.moviedbproject.model.MoviesResponse;
import com.b17dcat050.moviedbproject.model.TrailerResponse;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface Service {
    @GET("movie/popular")
    Call<MoviesResponse> getPopularMovies(@Query("api_key") String apiKey, @Query("page") int pageIndex);

    @GET("movie/now_playing")
    Call<MoviesResponse> getPlayingMovies(@Query("api_key") String apiKey, @Query("page") int pageIndex);

    @GET("movie/upcoming")
    Call<MoviesResponse> getUpcomingMovies(@Query("api_key") String apiKey, @Query("page") int pageIndex);

    @GET("movie/top_rated")
    Call<MoviesResponse> getTopMovies(@Query("api_key") String apiKey, @Query("page") int pageIndex);

    @GET("search/movie")
    Call<MoviesResponse> searchByName(@Query("api_key") String apiKey, @Query("query") String name, @Query("page") int pageIndex);

    @GET("genre/movie/list")
    Call<GenresResponse> getGenres(@Query("api_key") String apiKey);

    @GET("discover/movie")
    Call<MoviesResponse> searchByGenre(@Query("api_key") String apiKey, @Query("with_genres") String genre_id, @Query("page") int pageIndex);

    @GET("movie/{movie_id}/videos")
    Call<TrailerResponse> getMovieTrailer(@Path("movie_id") int id, @Query("api_key") String apiKey);
}
