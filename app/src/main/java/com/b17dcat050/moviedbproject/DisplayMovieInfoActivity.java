package com.b17dcat050.moviedbproject;

import androidx.appcompat.app.AppCompatActivity;
<<<<<<< HEAD
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.b17dcat050.moviedbproject.adapter.TrailerAdapter;
import com.b17dcat050.moviedbproject.api.Client;
import com.b17dcat050.moviedbproject.api.Service;
import com.b17dcat050.moviedbproject.model.Movie;
import com.b17dcat050.moviedbproject.model.Trailer;
import com.b17dcat050.moviedbproject.model.TrailerResponse;
import com.b17dcat050.moviedbproject.util.FavoriteContract;
import com.b17dcat050.moviedbproject.util.FavoriteDbHelper;
import com.bumptech.glide.Glide;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DisplayMovieInfoActivity extends AppCompatActivity {
    TextView plotSynopsis, releaseDate;
    ImageView imageView, favoriteButton;
    Movie movie;
    String thumbnail, movieName, synopsis, dateOfRelease;
    int movie_id;


    private RecyclerView recyclerView;
    private TrailerAdapter adapter;
    private List<Trailer> trailerList;

    private FavoriteDbHelper favoriteDbHelper;
    private final AppCompatActivity activity = DisplayMovieInfoActivity.this;
    private SQLiteDatabase mDb;
    private Movie favorite;

=======

import android.os.Bundle;

public class DisplayMovieInfoActivity extends AppCompatActivity {
>>>>>>> 6c34d5e (Project initialized)

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_movie_info);
<<<<<<< HEAD

        //Setting up toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        //Initializing db
        FavoriteDbHelper dbHelper = new FavoriteDbHelper(this);
        mDb = dbHelper.getWritableDatabase();

        //Creating views
        imageView = (ImageView) findViewById(R.id.thumbnail_image_header);
        plotSynopsis = (TextView) findViewById(R.id.plotsynopsis);
        releaseDate = (TextView) findViewById(R.id.releasedate);
        favoriteButton = (ImageView) findViewById(R.id.favorite_button);

        //Getting intent data
        Intent intentThatStartedThisActivity = getIntent();
        if (intentThatStartedThisActivity.hasExtra("movies")) {
            movie = getIntent().getParcelableExtra("movies");
            thumbnail = movie.getPosterPath();
            movieName = movie.getOriginalTitle();
            synopsis = movie.getOverview();
            dateOfRelease = movie.getReleaseDate();
            movie_id = movie.getId();

            //Showing data
            String poster = "https://image.tmdb.org/t/p/w500" + thumbnail;
            Glide.with(this).load(poster).into(imageView);
            plotSynopsis.setText(synopsis);
            releaseDate.setText(dateOfRelease);
            ((CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar)).setTitle(movieName);

        } else {
            Toast.makeText(this, "No API Data", Toast.LENGTH_SHORT).show();
        }

        initViews();


        //Handling favorite button
        if (Exists(movie_id)){
            favoriteButton.setImageResource(R.drawable.ic_favorite_selected);
        }
        favoriteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Exists(movie_id)){
                    favoriteButton.setImageResource(R.drawable.ic_favorite);
                    favoriteDbHelper = new FavoriteDbHelper(DisplayMovieInfoActivity.this);
                    favoriteDbHelper.deleteFavorite(movie_id);
                    Snackbar.make(favoriteButton, "Removed from Favorite",
                            Snackbar.LENGTH_SHORT).show();
                }
                else{
                    favoriteButton.setImageResource(R.drawable.ic_favorite_selected);
                    saveFavorite();
                    Snackbar.make(favoriteButton, "Added to Favorite",
                            Snackbar.LENGTH_SHORT).show();
                }
            }
        });
    }

    public boolean Exists(int searchItem) {

        String[] projection = {
                FavoriteContract.FavoriteEntry._ID,
                FavoriteContract.FavoriteEntry.COLUMN_MOVIEID,
                FavoriteContract.FavoriteEntry.COLUMN_TITLE,
                FavoriteContract.FavoriteEntry.COLUMN_POSTER_PATH,
                FavoriteContract.FavoriteEntry.COLUMN_PLOT_SYNOPSIS
        };
        String selection = FavoriteContract.FavoriteEntry.COLUMN_MOVIEID + " =?";
        String[] selectionArgs = { Integer.toString(searchItem) };
        String limit = "1";

        Cursor cursor = mDb.query(FavoriteContract.FavoriteEntry.TABLE_NAME, projection, selection, selectionArgs, null, null, null, limit);
        boolean exists = (cursor.getCount() > 0);
        cursor.close();
        return exists;
    }


    private void initViews(){
        trailerList = new ArrayList<>();
        adapter = new TrailerAdapter(this, trailerList);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view1);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        loadJSON();
    }

    private void loadJSON() {
        try {
            Client Client = new Client();
            Service apiService = Client.getClient().create(Service.class);
            Call<TrailerResponse> call = apiService.getMovieTrailer(movie_id, BuildConfig.THE_MOVIE_DB_API_TOKEN);
            call.enqueue(new Callback<TrailerResponse>() {
                @Override
                public void onResponse(Call<TrailerResponse> call, Response<TrailerResponse> response) {
                    List<Trailer> trailer = response.body().getResults();
                    recyclerView.setAdapter(new TrailerAdapter(getApplicationContext(), trailer));
                    recyclerView.smoothScrollToPosition(0);
                }

                @Override
                public void onFailure(Call<TrailerResponse> call, Throwable t) {
                    Log.d("Error", t.getMessage());
                }
            });
        } catch (Exception e) {
            Log.d("Error", e.getMessage());
            Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
        }
    }

    public void saveFavorite(){
        favoriteDbHelper = new FavoriteDbHelper(activity);
        favorite = new Movie();
        favorite.setId(movie_id);
        favorite.setOriginalTitle(movieName);
        favorite.setPosterPath(thumbnail);
        favorite.setOverview(synopsis);
        favoriteDbHelper.addFavorite(favorite);
=======
>>>>>>> 6c34d5e (Project initialized)
    }
}