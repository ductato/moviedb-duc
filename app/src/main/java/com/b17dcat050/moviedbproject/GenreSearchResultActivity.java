package com.b17dcat050.moviedbproject;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import com.b17dcat050.moviedbproject.adapter.PaginationAdapter;
import com.b17dcat050.moviedbproject.api.Client;
import com.b17dcat050.moviedbproject.api.Service;
import com.b17dcat050.moviedbproject.model.Genres;
import com.b17dcat050.moviedbproject.model.Movie;
import com.b17dcat050.moviedbproject.model.MoviesResponse;
import com.b17dcat050.moviedbproject.util.PaginationScrollListener;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GenreSearchResultActivity extends AppCompatActivity {
    private String GENRE_ID;
    PaginationAdapter adapter;
    LinearLayoutManager linearLayoutManager;
    RecyclerView rv;
    private static final int PAGE_START = 1;
    private int TOTAL_PAGES = 2;
    private int currentPage = PAGE_START;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    private Service movieService;
    Genres genre;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_genre_search_result);

        // Intent data handling
        Intent intentThatStartedThisActivity = getIntent();
        if (intentThatStartedThisActivity.hasExtra("genres")) {
            genre = getIntent().getParcelableExtra("genres");
            GENRE_ID = genre.getId().toString();
            TextView topText = (TextView)findViewById((R.id.topText));
            topText.setText(genre.getName());
        }

        // RecyclerView handling
        rv = (RecyclerView)findViewById(R.id.main_recycler);
        adapter = new PaginationAdapter(GenreSearchResultActivity.this);
        linearLayoutManager = new LinearLayoutManager(GenreSearchResultActivity.this, LinearLayoutManager.VERTICAL, false);
        rv.setLayoutManager(linearLayoutManager);
        rv.setItemAnimator(new DefaultItemAnimator());
        rv.setAdapter(adapter);
        rv.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage += 1;
                TOTAL_PAGES += 1;
                loadNextPage();
            }

            @Override
            public int getTotalPageCount() {
                return TOTAL_PAGES;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

        //init service and load data
        movieService = Client.getClient().create(Service.class);
        loadFirstPage();

        // Bottom Navigation Bar handling
        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById((R.id.bottomNavigation));
        MenuItem item = bottomNavigationView.getMenu().findItem(R.id.ic_list);
        item.setChecked(true);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.ic_movie:
                        Intent intent1 = new Intent(getApplicationContext(), LoadMoviesActivity.class);
                        startActivity(intent1);
                        break;

                    case R.id.ic_list:
                        break;

                    case R.id.ic_search:
                        Intent intent3 = new Intent(getApplicationContext(), SearchByNameActivity.class);
                        startActivity(intent3);
                        break;

                    case R.id.ic_favorite:
                        Intent intent4 = new Intent(getApplicationContext(), ShowFavoritesActivity.class);
                        startActivity(intent4);
                        break;
                }
                return false;
            }
        });
    }

    private void loadFirstPage() {
        searchByGenre().enqueue(new Callback<MoviesResponse>() {
            @Override
            public void onResponse(Call<MoviesResponse> call, Response<MoviesResponse> response) {
                // Got data -> Send to adapter

                List<Movie> results = fetchResults(response);
                adapter.addAll(results);

                if (currentPage <= TOTAL_PAGES) adapter.addLoadingFooter();
                else isLastPage = true;
            }

            @Override
            public void onFailure(Call<MoviesResponse> call, Throwable t) {
                t.printStackTrace();

            }
        });
    }

    private List<Movie> fetchResults(Response<MoviesResponse> response) {
        MoviesResponse movieResponse = response.body();
        return movieResponse.getResults();
    }

    private void loadNextPage() {
        searchByGenre().enqueue(new Callback<MoviesResponse>() {
            @Override
            public void onResponse(Call<MoviesResponse> call, Response<MoviesResponse> response) {
                adapter.removeLoadingFooter();
                isLoading = false;

                List<Movie> results = fetchResults(response);
                adapter.addAll(results);

                if (currentPage != TOTAL_PAGES) adapter.addLoadingFooter();
                else isLastPage = true;
            }

            @Override
            public void onFailure(Call<MoviesResponse> call, Throwable t) {
                t.printStackTrace();
                // No handling failure
            }
        });
    }

    private Call<MoviesResponse> searchByGenre() {
        return movieService.searchByGenre(
                BuildConfig.THE_MOVIE_DB_API_TOKEN,
                GENRE_ID,
                currentPage
        );
    }
}